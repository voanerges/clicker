﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCharacter : MonoBehaviour {


	public int MaxHealth = 100;
	public int CurrHealth = 100;
	public string Name = "SomeOne";
	public int EnemyPower = 10;
	public int Gold = 100;
	public bool isRuby;
	public float Speed = 2.0f;
	public bool IsDead;

	void Start(){
		GameManager.instance.SetEnemyUI (Name, CurrHealth, MaxHealth);
		IsDead = false;
		StartCoroutine(EnemyAttack());
	}

	public void GetDamage (int damage){
		int tempHealth = CurrHealth - damage;
		if (tempHealth <= 0) {
			
			GameManager.instance.CurrEnemy = null;
			GameManager.instance.SetPlayerGold (Gold, isRuby);
			IsDead = true;
			gameObject.SetActive (false);
			Destroy (gameObject);

		}
			
		CurrHealth = tempHealth;
		GameManager.instance.SetEnemyUI (Name, CurrHealth, MaxHealth);
	}

	IEnumerator EnemyAttack(){
		if (!IsDead) {
			GameObject playerHero = GameManager.instance.PlayerHero;
			playerHero.GetComponent<PlayerCharacter> ().GetDamage (EnemyPower);

			for (int i = 0; i < GameManager.instance.FriendsObjArray.Length; i++) {
				GameObject tempFriendObj = GameManager.instance.FriendsObjArray [i];
				if (tempFriendObj.activeSelf) {
					tempFriendObj.GetComponent<FriendCharacter> ().GetDamage (EnemyPower);
				
				}
			}
		}
			yield return new WaitForSeconds (Speed);

		if (!IsDead) {
			StartCoroutine (EnemyAttack ());
		}
		
	}
		

}
