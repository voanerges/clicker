﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {
	public AudioSource AudioSource;
	public AudioClip Sound01;
	public AudioClip Sound02;
	public static AudioController instance;
	public float SoundValume;

	void Awake(){
		instance = this;
	}

	void Start(){
		SoundValume = GameManager.instance.MenuObject.Sound.value;
		AudioSource.volume = GameManager.instance.MenuObject.Music.value;
	}

	public void HitSound(){
		AudioSource.PlayOneShot (Sound01, SoundValume);
	}

	public void HealSound(){
		AudioSource.PlayOneShot (Sound02, SoundValume);
	}

	public void MusicPlay(){
		AudioSource.Play();
	
	}

	public void MusicVolumeController(){
		AudioSource.volume = GameManager.instance.MenuObject.Music.value;
	}

	public void SoundVolumeController(){
		SoundValume = GameManager.instance.MenuObject.Sound.value;
	}


}
