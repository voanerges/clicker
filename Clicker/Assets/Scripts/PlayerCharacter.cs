﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerCharacter : MonoBehaviour {
	public string Name = "Укротитель драконов";
	public int AttackPower = 10;
	public int MaxHealth = 1000;
	public int CurrHealth = 1000;

	public Transform AttackEffectSpawn;
	public GameObject[] AttackEffectsArray;
	public Slider HealthUI;

	public bool IsDead;

	// Use this for initialization
	void Start () {
		IsDead = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AttackAction(){
		int index = Mathf.FloorToInt( Random.Range(0, AttackEffectsArray.Length));
		GetComponent<Animator>().SetTrigger ("Hit");
	
		GameObject newEffect = Instantiate (AttackEffectsArray [index], AttackEffectSpawn.position, AttackEffectSpawn.rotation, AttackEffectSpawn) as GameObject;
	
		Destroy (newEffect, 0.15f);
	}

	public void PowerUpSkills(int power){
		AttackPower += power;
		MaxHealth += power * 2;
		//CurrHealth += MaxHealth;
		HealthUI.value = (CurrHealth*1f / MaxHealth*1f);
	} 

	public void HillAction(){
		int tempHealth = CurrHealth + Mathf.FloorToInt(AttackPower/2.0f);

		if (tempHealth < MaxHealth) {
			CurrHealth = tempHealth;	

		}else CurrHealth = MaxHealth;
			HealthUI.value = (CurrHealth*1f / MaxHealth*1f);
	}


	public void GetDamage (int damage){
		int tempHealth = CurrHealth - damage;
		if (tempHealth <= 0) {
			IsDead = true;
			//GameManager.instance.MenuObject.ShowMenu ();
			//gameObject.SetActive (false);
			//Destroy (gameObject);
		}

		CurrHealth = tempHealth;
		HealthUI.value = (CurrHealth*1f / MaxHealth*1f);
	}
}
