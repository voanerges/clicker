﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public  class GameManager : MonoBehaviour {

	public int PlayerGold = 0;
	public int PlayerRuby = 0;
	public Text EnemyNameUI;
	public Slider EnemyHealthUI;
	public Text PlayerGoldUI;
	public Text PlayerRubyUI;
	public GameObject[] FriendsObjArray;
	public GameObject[] EnemyObjArray;
	public GameObject GoldObj;
	public GameObject RubyObj;
	public Transform EnemySpawnPoint;
	public MenuScript MenuObject;

	public GameObject PlayerHero;

	public Text PlayerAttackPowerUI;
	public Text DeadEnemiesScoreUI;

	public GameObject CurrEnemy;

	public float TotalProgress;
	public GameObject[] UniqueEnemyObjectArray;

	public static GameManager instance;

	public int enemyIndexCount= 0;

	public GameObject Base; 


	void Awake(){
		instance = this;
		CurrEnemy = null;
	}
	// Use this for initialization
	void Start () {
		UniqueEnemyObjectArray = new GameObject[EnemyObjArray.Length];

		SetNewEnemy ();
		SetPlayerPower (0);
		AudioController.instance.MusicPlay ();

	}
	
	// Update is called once per frame
	void LateUpdate () {
		
	}

	public void SetEnemyUI(string name, int currHealth, int maxHealth){
		EnemyNameUI.text = name;
		EnemyHealthUI.value = (currHealth*1f / maxHealth*1f);

	}

	public void SetPlayerGold(int gold, bool isRuby){
		//Debug.Log (gold);

			GameObject tempGoldObj = Instantiate (GoldObj, EnemySpawnPoint.position, EnemySpawnPoint.rotation, EnemySpawnPoint) as GameObject;
			Destroy (tempGoldObj, 2);
	
		if (isRuby) {
			GameObject tempRubyObj = Instantiate (RubyObj, EnemySpawnPoint.position, EnemySpawnPoint.rotation, EnemySpawnPoint) as GameObject;
			Destroy (tempRubyObj, 2);		
		}

		PlayerGold += gold;
		if (isRuby) {
			PlayerRuby += 1;
		}
		TotalProgress = (UniqueEnemyObjectArray.Length / EnemyObjArray.Length) * 1.0F;



		DeadEnemiesScoreUI.text = enemyIndexCount+"";
		SetNewEnemy ();	
	}

	public void SetNewEnemy(){
		if (CurrEnemy == null) {
			
			PlayerGoldUI.text = PlayerGold + "";
			PlayerRubyUI.text = PlayerRuby + "";

			int maxIndex;
			if (enemyIndexCount < EnemyObjArray.Length - 1)
				maxIndex = enemyIndexCount / 2 + 1;
			else
				maxIndex = EnemyObjArray.Length-1;




			int index = Mathf.FloorToInt (Random.Range (0, maxIndex));
			//if(EnemyObjArray[index] != null)
			if (EnemyObjArray.Length > index && index >= 0) {
				CurrEnemy = Instantiate (EnemyObjArray [index], new Vector3 (0, 0, 0), EnemySpawnPoint.rotation, EnemySpawnPoint) as GameObject;
				if (UniqueEnemyObjectArray[index] == null) {
					UniqueEnemyObjectArray[index] = EnemyObjArray [index];

				}
			}

			enemyIndexCount++;
		}
	}

	public void SetPlayerPower(int power){
		PlayerCharacter playerCharacter = PlayerHero.GetComponent<PlayerCharacter> ();
		playerCharacter.PowerUpSkills(power);
		PlayerAttackPowerUI.text = playerCharacter.AttackPower + "";
	}
		
}
