﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendBullet : MonoBehaviour {
	public int AttackPower;
	void Update(){
		AttackAction(AttackPower);
	
	}

	public void AttackAction(int attackPower){
		
		GameObject currEnemy =	GameManager.instance.CurrEnemy;
		if (currEnemy != null) {

		//	Debug.Log ("Friend Attack");
			transform.position = Vector2.MoveTowards (transform.position, currEnemy.transform.position, Time.deltaTime * 10);
			if (Vector2.Distance (transform.position, currEnemy.transform.position) < 0.1f) {
				currEnemy.GetComponent<GetHit> ().BulletHit (attackPower);
				Destroy (gameObject, 0f);
			}
		} else {
			Destroy (gameObject, 0f);
		
		}
	}
}
