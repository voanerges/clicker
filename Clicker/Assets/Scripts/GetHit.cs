﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetHit : MonoBehaviour {

	void OnMouseDown(){
		
		GameObject playerHero = GameManager.instance.PlayerHero;
		PlayerCharacter playerCharacter = playerHero.GetComponent<PlayerCharacter> ();

		if (!playerCharacter.IsDead) {
			playerCharacter.AttackAction ();
			GetComponent<Animator> ().SetTrigger ("Hit");
			AudioController.instance.HitSound ();
			GetComponent<EnemyCharacter> ().GetDamage (playerCharacter.AttackPower);
		} else if (playerCharacter.IsDead) {
			GameManager.instance.MenuObject.ShowMenuDead ();
		}
	}

	public void BulletHit(int attackPower){
		GetComponent<Animator>().SetTrigger ("Hit");
		AudioController.instance.HitSound ();
		GetComponent<EnemyCharacter> ().GetDamage(attackPower);
	
	}
}
