﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealHit : MonoBehaviour {

	void OnMouseDown(){
		PlayerCharacter ObjComponent = gameObject.GetComponent<PlayerCharacter> ();
		if (ObjComponent != null) {
			ObjComponent.HillAction ();
			AudioController.instance.HealSound ();
		}else {
			FriendCharacter ObjComponent2 = gameObject.GetComponent<FriendCharacter> ();
		 	ObjComponent2.HillAction ();
			AudioController.instance.HealSound ();
		}
	}
}
