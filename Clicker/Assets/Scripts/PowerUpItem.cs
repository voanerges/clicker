﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpItem : MonoBehaviour {
	public bool PayInRuby;
	public bool UpgradeFriend;

	public int Price;
	public int Power;
	public Sprite PowerUpIcon;
	public Text PriceUi;
	public Text PowerUi;
	public Image PowerUpIconUi;

	public GameObject FriendObj;

	// Use this for initialization
	void Start () {
		PriceUi.text = Price + "";
		PowerUi.text = "+ "+Power + "";
		PowerUpIconUi.sprite = PowerUpIcon;

	}

	public void OnClickAction(){
		if (GameManager.instance.PlayerGold >= Price
		    && !PayInRuby && !UpgradeFriend) {
			GameManager.instance.PlayerGold -= Price;
			GameManager.instance.PlayerGoldUI.text = GameManager.instance.PlayerGold + "";
			GameManager.instance.SetPlayerPower (Power);
			Price += Price;
			Power += Power;

			PriceUi.text = Price + "";
			PowerUi.text = "+ "+Power + "";
			//DestroyImmediate (gameObject, true);
			//gameObject.SetActive(false);
		} else if (GameManager.instance.PlayerRuby >= Price && PayInRuby && UpgradeFriend) {
			GameManager.instance.PlayerRuby -= Price;
			GameManager.instance.PlayerRubyUI.text = GameManager.instance.PlayerRuby + "";

			FriendObj.SetActive (true);
			FriendObj.GetComponent<FriendCharacter>().PowerUpSkills(Power);

			Price += Price;
			Power += Power;

			PriceUi.text = Price + "";
			PowerUi.text = "+ "+Power + "";
		
		}

	}



}
	