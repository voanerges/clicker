﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {

	public GameObject MenuGameObject;

	public Text Score;
	public Slider TotalProgress;
	public Text Persent;
	public Slider Music;
	public Slider Sound;
	public GameObject FightButton;
	public GameObject RestartButton;

	// Use this for initialization
	void Start () {
		MenuGameObject.SetActive (true);
		GameManager.instance.Base.SetActive (false);

		Score.text = GameManager.instance.enemyIndexCount+"";
		TotalProgress.value = GameManager.instance.TotalProgress;
		Persent.text = 0+" % ";
		RestartButton.SetActive (false);
		FightButton.SetActive (true);



	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void Fight(){
		HideMenu ();
	}

	public void Restart(){
		HideMenu ();
		Application.LoadLevel(Application.loadedLevel);
	}



	public void ShowMenuPouse(){
		Score.text = (GameManager.instance.enemyIndexCount-1)+"";
		TotalProgress.value = GameManager.instance.TotalProgress;
		Persent.text = GameManager.instance.TotalProgress + " % ";
		GameManager.instance.Base.SetActive (false);
		MenuGameObject.SetActive (true);
		RestartButton.SetActive (true);
	
	}

	public void ShowMenuDead(){
		Score.text = (GameManager.instance.enemyIndexCount-1)+"";
		TotalProgress.value = GameManager.instance.TotalProgress;
		Persent.text = GameManager.instance.TotalProgress + " % ";
		FightButton.SetActive (false);
		GameManager.instance.Base.SetActive (false);
		MenuGameObject.SetActive (true);
		RestartButton.SetActive (true);
	
	}

	public void HideMenu(){
		GameManager.instance.Base.SetActive (true);
		MenuGameObject.SetActive (false);

	}
}
