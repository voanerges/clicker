﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FriendCharacter : MonoBehaviour {
	public GameObject AttackEffectPrefab;
	public string FriendName ="Friend Name";
	public int AttackPower = 10;
	public float Speed = 2.0f;
	public int MaxHealth = 100;
	public int CurrHealth = 100;
	public Slider HealthUI;
	public bool IsDead;
	void Start(){
		HealthUI.gameObject.SetActive (true);
		IsDead = false;
		StartCoroutine(FriendAttack());

	}

	IEnumerator FriendAttack(){
		if(!IsDead)
			if (GameManager.instance.CurrEnemy != null) {
				GameObject newEffect = Instantiate (AttackEffectPrefab) as GameObject;
				newEffect.transform.position = gameObject.transform.position;

				newEffect.GetComponent<FriendBullet> ().AttackPower = AttackPower;
			}
				yield return new WaitForSeconds (Speed);
				
				StartCoroutine(FriendAttack());
		
	}

	public void PowerUpSkills(int power){
		AttackPower += power;
		MaxHealth += power * 2;
		//CurrHealth += MaxHealth;
		HealthUI.gameObject.SetActive (true);
		HealthUI.value = (CurrHealth*1f / MaxHealth*1f);

	}

	public void HillAction(){
		int tempHealth = CurrHealth + Mathf.FloorToInt(AttackPower);


			
		if (tempHealth < MaxHealth) {
			CurrHealth = tempHealth;	
		}else CurrHealth = MaxHealth;
		
		IsDead = false;
		HealthUI.value = (CurrHealth*1f / MaxHealth*1f);
	}

	public void GetDamage (int damage){
		int tempHealth = CurrHealth - damage;
		if (tempHealth <= 0) {
			IsDead = true;
			//gameObject.SetActive (false);
			//Destroy (gameObject);
		}

		CurrHealth = tempHealth;
		HealthUI.value = (CurrHealth*1f / MaxHealth*1f);
	}
}
